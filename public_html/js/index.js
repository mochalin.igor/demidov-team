const initMap = () => {
  createMap('.map-1', 55.840222, 37.645156, 14)
  createMap('.map-2', 55.635437, 37.728191, 14)
}

function createMap (selector, lat, lng, zoom = 14) {
  const uluru = {lat, lng};
  const element = document.querySelector(selector)
  const params = {
    zoom,
    center: uluru
  }

  const map = new google.maps.Map(element, params);
  const marker = new google.maps.Marker({
    position: uluru,
    map: map,
  });
}

window.initMap = initMap;
